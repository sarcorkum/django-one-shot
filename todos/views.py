from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, CreateForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo,
        "todo_list_list": todo,
    }
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todos)
        if form.is_valid():
            form.save()
            return redirect(todo_list_detail, id=id)
    else:
        form = TodoForm(instance=todos)

    context = {
        "todo_object": todos,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todos.delete()
        return redirect(todo_list_list)

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect(todo_list_detail, id=item.list.id)
    else:
        form = CreateForm()
    context = {
        "create_form": form,
    }
    return render(request, "todos/create_item.html", context)
